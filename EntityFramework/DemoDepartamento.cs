﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFramework.Dal;
using EntityFramework.Dal.Repositories;
using EntityFramework.Domain;

namespace EntityFramework
{
    public class DemoDepartamento
    {
        private readonly GenericRepository<Departamento> _repository;

        public DemoDepartamento()
        {
            _repository = new GenericRepository<Departamento>();
        }

        public void Executar()
        {
            try
            {
                Console.WriteLine("Iniciando " + GetType().Name);

                ExcluirRegistros();

                InserirRegistros();

                ConsultarRegistros();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }
        }

        private void ExcluirRegistros()
        {
            Console.WriteLine("Excluindo os registros existentes no BD..");

            IEnumerable<Departamento> registros = _repository.GetAll();

            _repository.DeleteRange(registros);

            _repository.SaveChanges();
        }

        public void InserirRegistros()
        {
            Console.WriteLine("Inserindo novas registros no BD..");

            Inserir(1, "Análise", 8080);
            Inserir(2, "SAC", 8081);
            Inserir(3, "Diretoria", null);

            _repository.SaveChanges();
        }

        public void Inserir(int id, string nome, int? ramal)
        {
            var registro = new Departamento
            {
                DepartamentoId = id,
                Nome = nome,
                Ramal = ramal
            };

            _repository.Insert(registro);
        }

        public void ConsultarRegistros()
        {
            Console.WriteLine("Consultando os registros existentes no BD..");

            var registros = _repository.GetAll();

            foreach (var registro in registros)
            {
                Console.WriteLine(" * Id: {0}, Nome: {1}, Ramal: {2}",
                                  registro.DepartamentoId, registro.Nome, registro.Ramal);
            }
        }
    }
}
