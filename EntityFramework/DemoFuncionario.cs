﻿using System;
using System.Collections.Generic;
using EntityFramework.Dal.Repositories;
using EntityFramework.Domain;

namespace EntityFramework
{
    public class DemoFuncionario
    {
        private readonly GenericRepository<Funcionario> _repository;

        public DemoFuncionario()
        {
            _repository = new GenericRepository<Funcionario>();
        }

        public void Executar()
        {
            try
            {
                Console.WriteLine("Iniciando " + GetType().Name);

                ExcluirRegistros();

                InserirRegistros();

                VincularFuncionario();

                ConsultarRegistros();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }
        }

        private void VincularFuncionario()
        {
            var deptoRepto = new GenericRepository<Departamento>(_repository.Context);
            Departamento depto1 = deptoRepto.Find(1);
            Departamento depto2 = deptoRepto.Find(2);

            Funcionario funcionario = _repository.Find(4);

            if (funcionario.Departamentos == null)
            {
                funcionario.Departamentos = new List<Departamento>();    
            }

            funcionario.Departamentos.Add(depto1);
            funcionario.Departamentos.Add(depto2);

            _repository.Update(funcionario);
            _repository.SaveChanges();
        }

        private void ExcluirRegistros()
        {
            Console.WriteLine("Excluindo os registros existentes no BD..");

            IEnumerable<Funcionario> registros = _repository.GetAll();

            _repository.DeleteRange(registros);

            _repository.SaveChanges();
        }

        public void InserirRegistros()
        {
            Console.WriteLine("Inserindo novas registros no BD..");

            Inserir(1, "Marcos", new DateTime(1995, 1, 1), Sexo.Masculino, new DateTime(2010,1,1), 1234.56m);

            _repository.SaveChanges();
        }

        public void Inserir(int id, string nome, DateTime nascimento, Sexo sexo, DateTime admissao, decimal salario)
        {
            var registro = new Funcionario
            {
                FuncionarioId = id,
                PessoaId = 4,
                Nome = nome,
                Nascimento = nascimento,
                Sexo = sexo,
                Admissao = admissao,
                Salario = salario
            };

            _repository.Insert(registro);
        }

        public void ConsultarRegistros()
        {
            Console.WriteLine("Consultando os registros existentes no BD..");

            var registros = _repository.GetAll();

            foreach (var registro in registros)
            {
                Console.WriteLine(" * Id: {0}, Nome: {1}, Admissao: {2:d}, Salário: {3:C}",
                                  registro.FuncionarioId, registro.Nome, registro.Admissao, registro.Salario);

                foreach (var depto in registro.Departamentos)
                {
                    Console.WriteLine(" --> Id: {0}, Nome: {1}, Ramal: {2}",
                                      depto.DepartamentoId, depto.Nome, depto.Ramal);
                }
            }
        }
    }
}
