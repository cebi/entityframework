﻿namespace EntityFramework
{
    public class Demo
    {
        public static void Executar()
        {
            //ExecutarDemoDepartamento();

            //ExecutarDemoPessoa();

            ExecutarDemoFuncionario();
        }

        private static void ExecutarDemoFuncionario()
        {
            var demo = new DemoFuncionario();
            demo.Executar();
        }

        private static void ExecutarDemoDepartamento()
        {
            var demo = new DemoDepartamento();
            demo.Executar();
        }

        private static void ExecutarDemoPessoa()
        {
            var demo = new DemoPessoa();
            demo.Executar();
        }


    }
}
