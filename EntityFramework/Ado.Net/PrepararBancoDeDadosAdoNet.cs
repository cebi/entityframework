﻿using System;
using System.Data.SQLite;

namespace EntityFramework
{
    public class PrepararBancoDeDadosAdoNet
    {
        private SQLiteConnection _conexao;

        public void Executar()
        {
            AbrirConexao();

            CriarTabela();

            InserirRegistros();

            ConsultarRegistros();

            EncerrarConexao();
        }

        private void AbrirConexao()
        {
            string connectString = "Data Source=Cebi.sqlite";

            _conexao = new SQLiteConnection(connectString);

            _conexao.Open();

            Console.WriteLine("Estado da conexão: " + _conexao.State);
        }

        private void CriarTabela()
        {
            string sql = "CREATE TABLE Pessoas (Id INTEGER PRIMARY KEY, Nome TEXT, Nascimento TEXT, Situacao INTEGER) ";

            var command = new SQLiteCommand(sql, _conexao);

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro: {0}", ex.Message);
            }
        }

        private void InserirRegistros()
        {
            InserirRegistro("Fulano", "2001-01-01", 1);
            InserirRegistro("Ciclano", "2002-02-02", 2);
            InserirRegistro("Beltrano", "2003-03-03", 3);
        }

        private void InserirRegistro(string nome, string nascimento, long situacao)
        {
            string sql = string.Format("INSERT INTO pessoas (Nome, Nascimento, Situacao) VALUES ('{0}', '{1}', {2})", nome, nascimento, situacao);

            var cmd = new SQLiteCommand(sql, _conexao);

            int rowsAffected = cmd.ExecuteNonQuery();

            Console.WriteLine("Linhas afetadas: {0}", rowsAffected);
        }

        private void ConsultarRegistros()
        {
            string SQL = "SELECT * FROM Pessoas";

            var cmd = new SQLiteCommand(SQL, _conexao);

            SQLiteDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string nome = (string)reader["Nome"];
                string nascimento = (string)reader["Nascimento"];
                long situacao = (long)reader["Situacao"];

                Console.WriteLine("Nome: {0}, Nascimento: {1}, Situacao: {2}", nome, nascimento, situacao);
            }

            reader.Close();
        }

        private void EncerrarConexao()
        {
            _conexao.Close();
        }
    }
}
