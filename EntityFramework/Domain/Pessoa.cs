﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework.Domain
{
    public enum Sexo
    {
        Masculino,
        Feminino  
    }

    public class Pessoa
    {
        public int PessoaId { get; set; }

        public string Nome { get; set; }

        public DateTime Nascimento { get; set; }

        public Sexo Sexo { get; set; }

    }
}
