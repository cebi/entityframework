﻿using System;
using System.Collections.Generic;

namespace EntityFramework.Domain
{
    public class Funcionario : Pessoa
    {
        public Funcionario()
        {
        }

        public Funcionario(Pessoa pessoa)
        {
            this.PessoaId = pessoa.PessoaId;
            this.Nome = pessoa.Nome;
            this.Nascimento = pessoa.Nascimento;
            this.Sexo = pessoa.Sexo;
        }

        public int FuncionarioId { get; set; }

        public DateTime Admissao { get; set; }

        public decimal Salario { get; set; }

        public virtual ICollection<Departamento> Departamentos { get; set; }
    }
}
