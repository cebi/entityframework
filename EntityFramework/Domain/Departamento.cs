﻿using System;
using System.Collections.Generic;

namespace EntityFramework.Domain
{
    public class Departamento
    {
        public int DepartamentoId { get; set; }

        public string Nome { get; set; }

        public int? Ramal { get; set; }

        public virtual ICollection<Funcionario> Funcionarios { get; set; }
    }
}
