﻿using System;
using System.Collections.Generic;
using EntityFramework.Dal.Repositories;
using EntityFramework.Domain;

namespace EntityFramework
{
    public class DemoPessoa
    {
        private readonly GenericRepository<Pessoa> _repository;

        public DemoPessoa()
        {
            _repository = new GenericRepository<Pessoa>();
        }

        public void Executar()
        {
            try
            {
                Console.WriteLine("Iniciando " + GetType().Name);

                ExcluirRegistros();

                InserirRegistros();

                ConsultarRegistros();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }
        }

        private void ExcluirRegistros()
        {
            Console.WriteLine("Excluindo os registros existentes no BD..");

            IEnumerable<Pessoa> registros = _repository.GetAll();

            _repository.DeleteRange(registros);

            _repository.SaveChanges();
        }

        public void InserirRegistros()
        {
            Console.WriteLine("Inserindo novas registros no BD..");

           Inserir(1, "Roberto", new DateTime(2001, 1, 1), Sexo.Masculino);
           Inserir(2, "Fernanda", new DateTime(2002, 1, 1), Sexo.Feminino);
           Inserir(3, "Denise", new DateTime(2003, 1, 1), Sexo.Feminino);

            _repository.SaveChanges();
        }

        public void Inserir(int id, string nome, DateTime nascimento, Sexo sexo)
        {
            var registro = new Pessoa
            {
                PessoaId = id,
                Nome = nome,
                Nascimento = nascimento,
                Sexo = sexo
            };

            _repository.Insert(registro);
        }

        public void ConsultarRegistros()
        {
            Console.WriteLine("Consultando os registros existentes no BD..");

            var registros = _repository.GetAll();

            foreach (var registro in registros)
            {
                Console.WriteLine(" * Id: {0}, Nome: {1}, Nascimento: {2}, Sexo: {3}",
                                  registro.PessoaId, registro.Nome, registro.Nascimento, registro.Sexo);
            }
        }
    }
}
