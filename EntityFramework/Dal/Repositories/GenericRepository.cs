﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;

namespace EntityFramework.Dal.Repositories
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        private readonly CebiContext _db;
        private readonly DbSet<TEntity> _dbSet; 

        public GenericRepository() : this(new CebiContext())
        {
        }

        public GenericRepository(CebiContext db)
        {
            _db = db;
            _dbSet = _db.Set<TEntity>();

            Console.WriteLine("Conexão: " + _db.Database.Connection.ConnectionString);
        }

        public CebiContext Context {
            get
            {
                return _db;
            }
        }

        public TEntity Find(int id)
        {
            return _dbSet.Find(id);
        }

        public void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            TEntity entity = Find(id);

            Delete(entity);
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }
    }
}
