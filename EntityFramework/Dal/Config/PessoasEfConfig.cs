﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EntityFramework.Domain;

namespace EntityFramework.Dal.Config
{
    public class PessoaEfConfig : EntityTypeConfiguration<Pessoa>
    {
        public PessoaEfConfig()
        {
            ToTable("Pessoas")
                .HasKey(x => x.PessoaId);

            Property(x => x.PessoaId)
                .HasColumnName("PessoaId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
