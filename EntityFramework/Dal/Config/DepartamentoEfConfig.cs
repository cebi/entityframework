﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EntityFramework.Domain;

namespace EntityFramework.Dal.Config
{
    public class DepartamentoEfConfig : EntityTypeConfiguration<Departamento>
    {
        public DepartamentoEfConfig()
        {
            ToTable("Departamentos")
                .HasKey(x => x.DepartamentoId)
                .HasMany(x => x.Funcionarios)
                .WithMany(x => x.Departamentos)
                .Map(m =>
                {
                    m.MapLeftKey("DepartamentoId");
                    m.MapRightKey("FuncionarioId");
                    m.ToTable("FuncionariosDepartamentos");
                });

            Property(x => x.DepartamentoId)
                .HasColumnName("DepartamentoId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}