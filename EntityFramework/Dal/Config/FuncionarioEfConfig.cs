﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EntityFramework.Domain;

namespace EntityFramework.Dal.Config
{
    public class FuncionarioEfConfig : EntityTypeConfiguration<Funcionario>
    {
        public FuncionarioEfConfig()
        {
            ToTable("Funcionarios")
                .HasKey(x => x.FuncionarioId);

            Property(x => x.FuncionarioId)
                .HasColumnName("FuncionarioId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
