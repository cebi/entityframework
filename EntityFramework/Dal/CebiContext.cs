﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using EntityFramework.Dal.Config;
using EntityFramework.Domain;

namespace EntityFramework.Dal
{
    public class CebiContext : DbContext
    {
        public CebiContext()
        {
            Database.SetInitializer<CebiContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // Configurations
            modelBuilder.Configurations.Add(new DepartamentoEfConfig());
            modelBuilder.Configurations.Add(new PessoaEfConfig());
            modelBuilder.Configurations.Add(new FuncionarioEfConfig());
        }

        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Funcionario> Funcionarios { get; set; }
    }
}
