﻿using System;
using EntityFramework.Dal;
using EntityFramework.Domain;

namespace EntityFramework
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Demo.Executar();

            Console.WriteLine("-- FIM --");
            Console.Read();
        }

        private static void CriarDepartamentos()
        {
            var db = new CebiContext();

            var depto1 = new Departamento {Nome = "Departamento 1"};
            var depto2 = new Departamento {Nome = "Departamento 2"};
            var depto3 = new Departamento {Nome = "Departamento 3"};

            db.Departamentos.Add(depto1);
            db.Departamentos.Add(depto2);
            db.Departamentos.Add(depto3);

            db.SaveChanges();
        }
    }
}